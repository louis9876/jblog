package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.BlogSystem;

public interface ISystemDAO extends IBaseDAO<BlogSystem>{
	
	/**
	 * 判断用户名密码是否正确
	 * @param username
	 * @param password
	 * @return
	 */
	boolean checkLogin(String username, String password);
	
	/**
	 * 通过key得到value
	 * @param key
	 * @return
	 */
	String getValue(String key);
	
	void update(String key, String value);
	
	int updatePwd(String oldPwd, String newPwd);
}

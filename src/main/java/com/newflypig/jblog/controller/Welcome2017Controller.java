package com.newflypig.jblog.controller;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.newflypig.jblog.model.Lianyihui;
import com.newflypig.jblog.model.Result;
import com.newflypig.jblog.service.ILianyihuiService;
import com.qiniu.util.StringUtils;

@Controller
public class Welcome2017Controller {
	
	@Resource(name = "lianyihuiService")
	ILianyihuiService lianyihuiService;
	
	@RequestMapping(value = "/welcome2017", method = RequestMethod.GET)  
    public ModelAndView getWelcome() { 
		return new ModelAndView("welcome2017");
	}
	
	@RequestMapping(value = "/welcome2017", method = RequestMethod.POST)  
    public ModelAndView postWelcome(Lianyihui lyh) {
		Result result = new Result();
		
		if(StringUtils.isNullOrEmpty(lyh.getRealname()) 
				|| StringUtils.isNullOrEmpty(lyh.getPhonenumber())
				|| !lyh.getRealname().matches("^([\u4e00-\u9fa5]{2,3})$")
				|| !lyh.getPhonenumber().matches("^1[358][0-9]{9}$")
		){
			result.setCode(Result.FAIL);
			result.setMessage("输入的内容不规范，请检查。");
		}
		
		lyh.setCreateDt(new Date());
		Integer id = (Integer) lianyihuiService.save(lyh);
		System.out.println(id);
		result.setData("{\"id\":\"" + String.format("%03d", id) + "\"}");
		
		return new ModelAndView("ajaxResult", "result", result);
	}
	
	@RequestMapping(value = "/memberlist", method = RequestMethod.GET)
	public ModelAndView memberList(){
		List<Lianyihui> members = this.lianyihuiService.findForShow();
		return new ModelAndView("memberlist", "members", members);
	}
	
	@RequestMapping(value = "/lianyihui/queryforedit/{hash}", method = RequestMethod.GET)
	public ModelAndView editGet(@PathVariable String hash){
		Lianyihui member = this.lianyihuiService.findByHash(hash);
		return new ModelAndView("memberedit", "member", member);
	}
	
	@RequestMapping(value = "/lianyihui/edit", method = RequestMethod.POST)
	public ModelAndView editPost(Lianyihui lyh){
		Lianyihui member = this.lianyihuiService.findById(lyh.getId());
		member.setPhonenumber(lyh.getPhonenumber());
		member.setRemark(lyh.getRemark());
		member.setRealname(lyh.getRealname());
		this.lianyihuiService.update(member);
		return new ModelAndView("ajaxResult", "result", new Result());
	}
}
